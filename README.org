#+title: Nix Dev Playground

Proof of concept for development tooling using [[https://nixos.org/][Nix]].

* Setup
  1. /Prequisite/: Setup [[https://direnv.net/][direnv]] on your system.
     1. Follow the [[https://direnv.net/docs/installation.html][install doc]]. You should use whichever package manager is relevant to your system, but there's a =curl | sh= option available as well.
     2. [[https://direnv.net/docs/hook.html][Hook direnv into your shell]].
  3. Run =direnv allow= in the project.
  4. That's it. Really. Now whenever you switch to this project's directory in a terminal it will automatically activate a reproducible development environment for you using Nix.
